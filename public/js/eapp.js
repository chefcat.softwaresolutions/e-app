'use strict';
angular.module('eapp', ['eapp.controllers','eapp.services'
	]).config(['$interpolateProvider'
	,function($interpolateProvider)
	{
		$interpolateProvider.startSymbol('{!');
  		$interpolateProvider.endSymbol('!}');
	}]);