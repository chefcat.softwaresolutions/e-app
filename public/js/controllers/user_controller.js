angular.module('eapp.controllers', ['blockUI'])
  .controller('UserController', UserController);

  UserController.$inject = ['$scope','blockUI','userService'];

  function UserController($scope,blockUI,userService)
  {
  	var self = this;
  	self.reg = {};
  	self.registerUser = registerUser;
  	self.errorHandler = errorHandler;
  	self.redirectLogin = redirectLogin;
  	self.login = login;

  	$scope.ui_block = ui_block;
  	$scope.ui_unblock = ui_unblock;

  	function ui_block()
  	{
  		blockUI.start();
  	}

  	function ui_unblock()
  	{
  		blockUI.stop();
  	}

  	function errorHandler(errors) {
	    self.errors = [];
	    if(angular.isArray(errors)){
	    	angular.forEach(errors, function(value, key){
	    		self.errors[key] = value.message;
	    	});
	    }else{
	    	self.errors[0] = value.message;
	    }
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return self.errors;
  }
  	function registerUser()
  	{
  		self.errors = Constants.FALSE;
  		self.success = Constants.FALSE;

  		self.reg.web = true;

  		$scope.ui_block();
  		userService.registerUser(self.reg).success(function(response){
  			if(response.status == 400){
  				self.errors = self.errorHandler(response.data.errors);
  			}
  			else if(response.status == 200){
  				self.redirectLogin();
  			}
  		$scope.ui_unblock();
  		}).error(function(response){
  			self.errors = self.errorHandler('Internal Server Error');
  			$scope.ui_unblock();
  		});

  	}
  	function redirectLogin()
  	{
  		window.location.replace('/users/login');
  		self.success = "Registration Complete, You can now login."
  	}

  	function login(){
  		self.errors = Constants.FALSE;
  		self.success = Constants.FALSE;

  		$scope.ui_block();
  		userService.login(self.email,self.password,self.user_type).success(function(response){
  			if(response.status == 400){
  				self.errors = self.errorHandler(response.data.errors);
  			}
  			else if(response.status == 200){
  				self.redirectLogin();
  			}
  			$scope.ui_unblock();
  		}).error(function(response){
  			$scope.ui_unblock();
  		});
  	}
  }