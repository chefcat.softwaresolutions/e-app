var services = angular.module('eapp.services',[]);
	services.factory('userService', function($http){
		var userApi = {};

		userApi.registerUser = function(reg){
			return $http({
				method : 'POST'
				, data : reg
				, url : '/api/v1/register'
			});
		}

		userApi.login = function(email, pass, type){
			return $http({
				method : 'POST'
				, data : {email : email, password : pass, user_type : type}
				, url  : '/api/v1/login'
			});
		}
		return userApi;
	});