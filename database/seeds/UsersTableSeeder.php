<?php
use FutureEd\Models\Core\User;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class UsersTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('users')->truncate();
        \DB::table('users')->insert(
        [
            [
            	'email' => 'franzbrianbriones@gmail.com'
            	, 'first_name' => 'Franz'
            	, 'last_name' => 'Briones'
            	, 'city' => 'Cebu'
            	, 'country' => 'Philippines'
            	, 'user_type' => 'sender'
            	, 'password' => Hash::make('test123')
            ]
        ]);
    }
}