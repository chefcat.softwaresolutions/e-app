<?php

namespace natEmergency\Services;

use natEmergency\User;

class UserServices{

	public function addUser($input){
		$user = new User;
		try{
			$user->email = $input['email'];
			$user->first_name = $input['first_name'];
			$user->last_name = $input['last_name'];
			$user->city = $input['city'];
			$user->country = $input['country'];
			$user->province = $input['province'];
			$user->user_type = ($input['user_type']) ? $input['user_type'] : 'user';
			$user->password = \Hash::make($input['password']);

			$user->save();
		}
		catch(Exception $e){
			return $e->getMessage();
		}
		
		return $user->id;
	}

	public function checkLogin($email,$password,$user_type){
		return \Auth::attempt(['email' => $email, 'password' => $password, 'user_type' => $user_type]);		
	}
}