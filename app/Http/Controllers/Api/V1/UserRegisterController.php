<?php

namespace natEmergency\Http\Controllers\Api\V1;

use natEmergency\User;

use natEmergency\Http\Controllers\Controller;
use natEmergency\Http\Controllers\Api\Traits\UserValidatorTrait;

use Illuminate\Support\Facades\Input;

use natEmergency\Services\UserServices;

class UserRegisterController extends Controller
{

	use UserValidatorTrait;

	public function __construct(UserServices $user)
	{
		$this->user = $user;
	}
	/**
	*@return User id
	*@param [array]
	*/
	public function register($is_web = false)
	{
		$input = Input::only('email', 'first_name', 'last_name', 'country', 'province', 'city', 'password','user_type','department_name');

		$is_web = Input::only('web');

		$this->addErrorMsg($this->validateEmail('email', $input));
		$this->addErrorMsg($this->validateString('first_name', $input));
		$this->addErrorMsg($this->validateString('last_name', $input));
		$this->addErrorMsg($this->validateString('country', $input));
		$this->addErrorMsg($this->validateString('province', $input));
		$this->addErrorMsg($this->validateString('city', $input));
		$this->addErrorMsg($this->validateString('department_name', $input));
		$this->addErrorMsg($this->validateString('password', $input));

		if($is_web == true)
		{
			$this->addErrorMsg($this->validateUserType('user_type', $input));
		}

		if($this->getErrormsg()){
			$response = 
				[
					'status' => 400,
					'data' => 
						[
							'errors' => $this->getErrormsg()
						]
				];
			return $response;
		}else
		{
			$adduser = $this->user->addUser($input);

			$response = 
			[
				'status' => 200,
				'data' =>
				[
					'success' => true
				]
			];

			return $response;
		}	

	}
}
  