<?php 

namespace natEmergency\Http\Controllers\Web;

use natEmergency\Http\Requests;
use natEmergency\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class UserRegisterController extends Controller {

	public function register() {
		return view('user.register');
	}
}