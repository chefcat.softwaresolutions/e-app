<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::post('api/v1/register', 'Api\V1\UserRegisterController@register');
Route::post('api/v1/login', 'Api\V1\UserLoginController@login');














// Front-end ROUTES

Route::get('/users/register', [
	'as' => 'user.register'
	, 'uses' => 'Web\UserRegisterController@register']);
Route::get('/users/login', [
	'as' => 'user.login'
	, 'uses' => 'Web\UserLoginController@login']);
