@extends('user.app')

@section('body')
	<div class="container" ng-cloak>
		<form class="form-signup form-horizontal" method="POST">
			<div class="container">
				<div class="col-md-8 col-md-offset-4">
					<h2 class="form-signup-heading">LOGIN</h2>
				</div>
				<div class="col-md-6 col-md-offset-3">
					<div class="alert alert-danger" ng-if="user.errors">
			            <p ng-repeat="error in user.errors track by $index" > 
			              	{! error !}
			            </p>
			        </div>
				</div>
				<div class="col-md-8 col-md-offset-2">
					<label class="col-md-4 control-label" for="email">Email</label>
					<div class="col-md-8">
						<input class="form-control" ng-model="user.email" name="email" id="email" placeHolder="Email"></input>
					</div>
				</div>
				<div class="col-md-8 col-md-offset-2">
					<label class="col-md-4 control-label" for="password">Password</label>
					<div class="col-md-8">
						<input type="password" class="form-control" ng-model="user.password" name="password" id="password" placeHolder="Password"></input>
					</div>
				</div>
				<div class="col-md-8 col-md-offset-2">
					<label class="col-md-4 control-label" for="user_type">User Type</label>
					<div class="col-md-8">
						<select class="form-control" ng-model="user.user_type"name="user_type" id="user_type">
							<option value="">-- Select Option --</option>
							<option value="bfp">BFP</option>
							<option value="hospital">Hospital</option>
						</select>
					</div>
				</div>
				<div class="col-md-8 col-md-offset-2">
              		<p>
              			<button type="button" class="btn btn-primary btn-medium" ng-click="user.login()">Login</button>
              		<p>{!! Html::link(route('user.register'), 'Sign Up') !!}</p>
				</div>
			</div>
		</form>
	</div>
@stop