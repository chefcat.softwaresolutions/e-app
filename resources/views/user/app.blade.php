<!DOCTYPE html>
<html lang="en" ng-app="eapp">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS -->
		{!! Html::style('/css/bootstrap.min.css') !!}
		{!! Html::style('//angular-block-ui.nullest.com/angular-block-ui/angular-block-ui.css') !!}
		@yield('styles')
	</head>

	<body ng-controller="UserController as user">
		@yield('body')
		@show
		{!! Html::script('//code.jquery.com/jquery-1.11.3.min.js') !!}
		{!! Html::script('//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.13/angular.min.js') !!}
	    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.13/angular-resource.min.js') !!}
	    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-cookies.min.js') !!}
		{!! Html::script('/js/eapp.js') !!}
		{!! Html::script('/js/constants.js') !!}
		{!! Html::script('/js/controllers/user_controller.js') !!}
		{!! Html::script('/js/services/user_service.js') !!}
		{!! Html::script('//angular-block-ui.nullest.com/angular-block-ui/angular-block-ui.js') !!}
	    @yield('scripts')
	</body>
</html>