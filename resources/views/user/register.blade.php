@extends('user.app')

@section('body')
	<div class="container">
			{!! Form::open(['class' => 'form-horizontal']) !!}

			
			<div class="container" ng-cloak>
	<div class="container">
		<form class="form-signup form-horizontal" method="POST" action="register">
			<div class="container">
				<div class="col-md-8 col-md-offset-4">
					<h2 class="form-signup-heading" style="font-family:Arial Black">REGISTRATION</h2>
				</div>
				<div class="col-md-6 col-md-offset-3">
					<div class="alert alert-danger" ng-if="user.errors">
			            <p ng-repeat="error in user.errors track by $index" > 
			              	{! error !}
			            </p>
			        </div>
				</div>
					<div class="col-md-6 col-md-offset-3">
					<br/>
					<label style="color:#B6B6B4; font-family: Calibri; font-size=12">Department Type </label>
					<div class="col-xs-8 col-xs-offset-2">
						<div class="form-group">
							<div class="col-xs-6">
								<input type="radio" ng-model="user.reg.user_type" checked="checked" name="department" value="hospital">Hospital</input>
							</div>
							<div class="col-xs-6">
								<input type="radio" ng-model="user.reg.user_type" name="department" value="bfp">BFP</input>
							</div>
						</div>	
					</div>				
					<br/>
				</div>

				<!--<div class="col-md-8 col-md-offset-4">--
					<!-- <label class="col-md-4 control-label" for="department_name">Deparment Name</label> -->
					<div class="col-md-6 col-md-offset-3">
					<br/>
						<input class="form-control" ng-model="user.reg.department_name" name="department_name" id="department_name" placeHolder="Department Name"></input>
					<br/>
					</div>

					<div class="col-md-6 col-md-offset-3">
					<br/>
						<input class="form-control" ng-model="user.reg.first_name" name="first_name" id="first_name" placeHolder="First Name"></input>
					<br/>
					</div>

					<div class="col-md-6 col-md-offset-3">
					<br/>
						<input class="form-control" ng-model="user.reg.last_name" name="last_name" id="last_name" placeHolder="Last Name"></input>
					<br/>
					</div>
				<!--</div>-->

				<!--<div class="col-md-8 col-md-offset-2">-->
					<!-- <label class="col-md-4 control-label" for="city">City</label> -->
					<div class="col-md-6 col-md-offset-3">
						<input class="form-control" ng-model="user.reg.city" name="city" id="city" placeHolder="City"></input>
					<br/>
					</div>
				<!--</div>-->

				<!--<div class="col-md-8 col-md-offset-2-->				
					<!-- <label class="col-md-4 control-label" for="province">Province</label> -->
					<div class="col-md-6 col-md-offset-3">
						<input class="form-control" ng-model="user.reg.province" name="province" id="province" placeHolder="Province"></input>
					<br/>
					</div>
				<!--</div>-->

				<!--<div class="col-md-8 col-md-offset-2">-->
					<!-- <label class="col-md-4 control-label" for="country">Country</label> -->
					<div class="col-md-6 col-md-offset-3">
						<input class="form-control" ng-model="user.reg.country" name="country" id="country" placeHolder="Country"></input>
					<br/>
					</div>
				<!--</div>-->

				<!--<div class="col-md-8 col-md-offset-2">-->
					<!-- <label class="col-md-4 control-label" for="email">Email</label> -->
					<div class="col-md-6 col-md-offset-3">
						<input class="form-control" ng-model="user.reg.email" name="email" id="email" placeHolder="Email"></input>
					<br/>
					</div>
				<!--</div>-->

				<!--<div class="col-md-8 col-md-offset-2">-->
					<!-- <label class="col-md-4 control-label" for="password">Password</label> -->
					<div class="col-md-6 col-md-offset-3">
						<input type="password" ng-model="user.reg.password" class="form-control" name="password" id="password" placeHolder="Password"></input>
					<br/>
					</div>
				<!--</div>-->
				<div class="col-md-4 col-md-offset-4">
					
              		<p>	
              			<a href="{!! route('user.login') !!}" class="btn btn-danger btn-medium">Cancel</a>
              			<button type="button" ng-click="user.registerUser()" name="submit" class="btn btn-primary btn-medium">Submit</button>
              		</p>
				</div>
			</div>
		</form>
	</div>
@stop